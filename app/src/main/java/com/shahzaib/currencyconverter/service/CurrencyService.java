package com.shahzaib.currencyconverter.service;

import com.shahzaib.currencyconverter.pojo.CurrencyRateResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CurrencyService {

    @GET("latest")
    Observable<CurrencyRateResponse> getCurrency(@Query("base") String base);

}

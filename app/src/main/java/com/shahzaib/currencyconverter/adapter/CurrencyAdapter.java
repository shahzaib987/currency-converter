package com.shahzaib.currencyconverter.adapter;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.shahzaib.currencyconverter.R;
import com.shahzaib.currencyconverter.pojo.CurrencyRateResponse;
import com.shahzaib.currencyconverter.pojo.CurrencyRate;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.MyViewHolder> {

    String ICON_PREFIX = "ic_";
    String LABEL_SUFFIX = "_lbl";
    String DRAWABLE = "drawable";
    String STRING = "string";
    String RATE_DATE_FORMAT = "#,###.##";


    DecimalFormat decimalFormat = new DecimalFormat(RATE_DATE_FORMAT);
    CurrencyRate selectedCurrencyRate;
    List<CurrencyRate> currencyRates;
    RecyclerView.LayoutManager layoutManager;
    Double multiplier = 1.0;


    OnRecycleEventChange onRecycleEventChange;

    public CurrencyAdapter(List<CurrencyRate> currencyRates, OnRecycleEventChange onRecycleEventChange) {

        this.currencyRates = currencyRates;
        this.onRecycleEventChange = onRecycleEventChange;
        selectedCurrencyRate = new CurrencyRate("EUR", 1.0);
        currencyRates.add(0, selectedCurrencyRate);

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new MyViewHolder(layoutInflater.inflate(R.layout.item_currancy, parent, false), new RateChangeTextWatcher());
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        //selected Value
        if (position == 0) {

            holder.bind(selectedCurrencyRate, position);
        } else {
            holder.bind(currencyRates.get(position), position);
        }
    }

    @Override
    public int getItemCount() {
        return currencyRates.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        layoutManager = recyclerView.getLayoutManager();
    }


    public void setCurrencyRates(CurrencyRateResponse currencyRateResponseRates) {

        this.currencyRates = currencyRateResponseRates.getRates();
        this.currencyRates.set(0, selectedCurrencyRate);

        notifyItemRangeChanged(1, currencyRateResponseRates.getRates().size());
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        View itemView;
        EditText etRate;
        TextView tvName;
        TextView tvSymbol;
        ImageView ivFlag;

        RateChangeTextWatcher rateChangeTextWatcher;

        public MyViewHolder(@NonNull View itemView, RateChangeTextWatcher rateChangeTextWatcher) {
            super(itemView);
            this.itemView = itemView;
            this.rateChangeTextWatcher = rateChangeTextWatcher;
            etRate = itemView.findViewById(R.id.et_rate);
            ivFlag = itemView.findViewById(R.id.iv_flag);
            tvName = itemView.findViewById(R.id.tv_name);
            tvSymbol = itemView.findViewById(R.id.tv_symbol);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getLayoutPosition();
                    updateSelectedRate(position);

                }
            });
        }


        public void bind(CurrencyRate currencyRate, int position) {
            Double value = currencyRate.getRate();
            Log.e("position", "" + position);
            if (position != 0) {
                value = multiplier * currencyRate.getRate();
            }


            etRate.setTag("" + value);
            etRate.setText(decimalFormat.format(value).toString());
            etRate.setTag(null);
            etRate.addTextChangedListener(rateChangeTextWatcher);


            tvName.setText(currencyRate.getCurrency());


            int txtResource = itemView.getResources().getIdentifier(currencyRate.getCurrency().toLowerCase() + LABEL_SUFFIX, STRING, itemView.getContext().getPackageName());
            tvSymbol.setText(txtResource);

            etRate.clearFocus();


            int imageResource = itemView.getResources().getIdentifier(ICON_PREFIX + currencyRate.getCurrency().toLowerCase(), DRAWABLE, itemView.getContext().getPackageName());
            Picasso.get().load(imageResource).into(ivFlag);
            rateChangeTextWatcher.setRate(currencyRate);
            rateChangeTextWatcher.setEditRate(etRate);


            etRate.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        int position = getLayoutPosition();
                        updateSelectedRate(position);
                    }
                    return false;
                }
            });


        }
    }


    private void updateSelectedRate(int position) {
        if (position != 0) {

            selectedCurrencyRate = currencyRates.get(position);
            layoutManager.scrollToPosition(0);
            Collections.swap(currencyRates, position, 0);
            notifyItemMoved(position, 0);

            onRecycleEventChange.dataChange(selectedCurrencyRate, position);

        }
    }

    class RateChangeTextWatcher implements TextWatcher {

        EditText editRate;
        CurrencyRate rate;


        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {


            if (editRate.getTag() == null && !s.toString().isEmpty()) {
                selectedCurrencyRate = rate;
                Log.e("Tag", selectedCurrencyRate.getCurrency() + " " + s.toString());

                selectedCurrencyRate.setRate(Double.parseDouble(s.toString()));

                multiplier = Double.parseDouble(s.toString().replace(",", ""));

                new Handler().postDelayed(() -> {
                    notifyItemRangeChanged(1, currencyRates.size());
                }, 500);

            }

        }


        @Override
        public void afterTextChanged(Editable s) {

        }

        public void setEditRate(EditText editRate) {
            this.editRate = editRate;
        }

        public void setRate(CurrencyRate rate) {
            this.rate = rate;
        }
    }


    public interface OnRecycleEventChange {
        void dataChange(CurrencyRate currencyRate, int position);

        void updatingChange(CurrencyRate currencyRate);

    }


}

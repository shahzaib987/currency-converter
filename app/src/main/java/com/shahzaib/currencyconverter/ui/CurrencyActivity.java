package com.shahzaib.currencyconverter.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.shahzaib.currencyconverter.R;
import com.shahzaib.currencyconverter.adapter.CurrencyAdapter;
import com.shahzaib.currencyconverter.pojo.CurrencyRate;
import com.shahzaib.currencyconverter.viewModels.CurrencyViewModel;


public class CurrencyActivity extends AppCompatActivity implements CurrencyAdapter.OnRecycleEventChange {

    private CurrencyViewModel currencyViewModel;


    RecyclerView recyclerViewCurrency;
    boolean isFirstTime = true;
    CurrencyAdapter adapter = null;
    String baseSelected = "EUR";
    boolean isSelected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        recyclerViewCurrency = findViewById(R.id.recyclerViewCurrancy);
        recyclerViewCurrency.setLayoutManager(new LinearLayoutManager(this));

        currencyViewModel = new ViewModelProvider(this).get(CurrencyViewModel.class);
        currencyViewModel.runStreamer();

        currencyViewModel.setBase(baseSelected);

        currencyViewModel.getCurrencyData().observe(this, (currency) -> {

            if (isFirstTime) {
                isFirstTime = false;
                adapter = new CurrencyAdapter(currency.getRates(), CurrencyActivity.this);
                recyclerViewCurrency.setAdapter(adapter);

            } else {
                if (isSelected) {
                    if (baseSelected.equalsIgnoreCase(currency.getBaseCurrency())) {
                        isSelected = false;
                        adapter.setCurrencyRates(currency);
                    }
                } else {
                    adapter.setCurrencyRates(currency);
                }
            }

        });

    }


    @Override
    public void dataChange(CurrencyRate currencyRate, int position) {
        isSelected = true;
        baseSelected = currencyRate.getCurrency();
        currencyViewModel.setBase(baseSelected);
    }

    @Override
    public void updatingChange(CurrencyRate currencyRate) {

    }
}

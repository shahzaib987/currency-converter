package com.shahzaib.currencyconverter.pojo;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

public class CurrencyRateResponse {
    @JsonProperty("baseCurrency")
    private String baseCurrency;

    @JsonProperty("rates")
    private HashMap<String,Double> rates;


    public String getBaseCurrency() {
        return baseCurrency;
    }

    public List<CurrencyRate> getRates() {
        List<CurrencyRate> currencyRates = new ArrayList<>();

        SortedSet<String> keys = new TreeSet<>(rates.keySet());
        for(String key:keys){
            CurrencyRate currencyRate = new CurrencyRate(key,rates.get(key));
            currencyRates.add(currencyRate);
        }
        return currencyRates;
    }




}

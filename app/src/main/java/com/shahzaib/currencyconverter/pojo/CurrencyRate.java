package com.shahzaib.currencyconverter.pojo;

public class CurrencyRate {


    String currency;
    Double rate;

    public CurrencyRate(String currency, Double rate) {
        this.currency = currency;
        this.rate = rate;
    }

    public String getCurrency() {
        return currency;
    }

    public Double getRate() {
        return rate;
    }


    public void setRate(Double rate) {
        this.rate = rate;
    }
}

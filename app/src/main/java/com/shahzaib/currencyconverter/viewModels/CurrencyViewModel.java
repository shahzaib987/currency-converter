package com.shahzaib.currencyconverter.viewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.shahzaib.currencyconverter.pojo.CurrencyRateResponse;
import com.shahzaib.currencyconverter.repositories.CurrencyRepository;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;


public class CurrencyViewModel extends ViewModel {

    private MutableLiveData<CurrencyRateResponse> currencyData;
    private CurrencyRepository currencyRepository;

    private String base = "EUR";

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public CurrencyViewModel() {
        currencyRepository = new CurrencyRepository();
    }

    public LiveData<CurrencyRateResponse> getCurrencyData() {
        currencyData = currencyRepository.getCurrencyData();
        return currencyData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }

    public void setBase(String base) {
        this.base = base;
    }

    public void runStreamer() {
        Disposable disposable = Observable.interval(1000, 1000,
                TimeUnit.MILLISECONDS)
                .switchMap((Function<Long, ObservableSource<?>>) aLong -> currencyRepository.getCurrency(base)).observeOn(AndroidSchedulers.mainThread()).subscribe();
        compositeDisposable.add(disposable);
    }
}

package com.shahzaib.currencyconverter.repositories;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.shahzaib.currencyconverter.pojo.CurrencyRateResponse;
import com.shahzaib.currencyconverter.service.CurrencyService;
import com.shahzaib.currencyconverter.service.RetrofitService;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CurrencyRepository {
    private CurrencyService currencyService;

    public CurrencyRepository() {
        currencyService = RetrofitService.createService(CurrencyService.class);
    }

    MutableLiveData<CurrencyRateResponse> currencyMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<CurrencyRateResponse> getCurrencyData() {
        return currencyMutableLiveData;
    }

    public Observable<CurrencyRateResponse> getCurrency(String base) {
        Observable<CurrencyRateResponse> observable = currencyService.getCurrency(base);
        observable.subscribeOn(Schedulers.newThread()).
                observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleResults, this::onError);
        return observable;
    }

    private void handleResults(CurrencyRateResponse currencyRateResponse) {
        currencyMutableLiveData.setValue(currencyRateResponse);
    }

    private void onError(Throwable throwable) {
        Log.e("error MV", throwable.getLocalizedMessage());
    }
}
